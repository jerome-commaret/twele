<h1>Twitter + TV using meteor.js</h1>
- The video stream is unsing inline media playback so this is only working on webkit based browser
- This includes IOS 10 devices and the latest version of safari for more information visit
 https://webkit.org/blog/6784/new-video-policies-for-ios/
- We are using HTTP Live Streaming for the video
- My developer certificate have been revoked and i am not wealthy to reconduct it

<h2> To run it use : </h2>
- <code>git clone git@gitlab.com:TheCreativeCat/twele.git</code>
- <code>cd twele</code>
- <code>meteor run</code>

<h2> To do : </h2>
- Implement tweets for mobile using react-twitt over stream Api
- Implement the abilities to tweet
- Store tweets to do some big data over it
- Implement Deep Text https://code.facebook.com/posts/181565595577955/introducing-deeptext-facebook-s-text-understanding-engine/
- Train Deep text with dictionary
- Develop the front-end solution to be able to show the results

<h2> Pitch the project : </h2>
- To People
- To Brands
- To TV networks
- To Twitter
