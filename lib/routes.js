Router.route('/', function () {
    this.render('index');
  },
  { name: 'Homepage'
});

Router.route('/channels', function () {
    this.render('channels');
  },
  { name: 'channels'
});

Router.route('/tf1', function () {
    this.render('tf1');
  },
  { name: 'tf1'
});
Router.route('/france2', function () {
    this.render('france2');
  },
  { name: 'france2'
});
Router.route('/france3', function () {
    this.render('france3');
  },
  { name: 'france3'
});
