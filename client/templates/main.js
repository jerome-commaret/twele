import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './main.html';
import './index.html';
import './france2.html';
import './france3.html';
import './channels.html';
